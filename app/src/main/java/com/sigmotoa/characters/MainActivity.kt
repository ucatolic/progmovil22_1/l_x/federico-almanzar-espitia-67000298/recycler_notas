package com.sigmotoa.characters

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button:Button = findViewById(R.id.buttonf)

        button.setOnClickListener{
            val builder = AlertDialog.Builder(this@MainActivity)
            val view = layoutInflater.inflate(R.layout.dialog, null)

            builder.setView(view)

            val dialog = builder.create()
            dialog.show()
        }

        //Recupera los datos de la fuente de datos
        val characterList = DataSource(this).getCharacterList()
        val characterListNota = DataSource(this).getCharacterListNot()

        val recyclerView:RecyclerView=findViewById(R.id.reciclerview)
        recyclerView.adapter=CharacterAdapter(characterList, characterListNota )
    }
}