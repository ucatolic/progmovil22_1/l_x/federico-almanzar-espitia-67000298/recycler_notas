package com.sigmotoa.characters
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

class CharacterAdapter (val characterList:Array<String>, val characterListNot:Array<String>):
    RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>()
{

    //Describe el item para el recyclerview
    class CharacterViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {
        private val characterTextView:TextView=itemView.findViewById(R.id.tvNombreMateria)
        private val characterTextView_nota:TextView=itemView.findViewById(R.id.tvNota2)

        fun bind(word: String, word2: String){
            characterTextView.text=word
            characterTextView_nota.text=word2
        }

    }

        // Retorna un nuevo ViewHolder
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CharacterAdapter.CharacterViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.character_item, parent, false)
        return CharacterViewHolder(view)
    }

    // Muestras los datos/items en las respectivas posiciones
    override fun onBindViewHolder(holder: CharacterAdapter.CharacterViewHolder, position: Int) {
        holder.bind(characterList[position], characterListNot[position])
    }

    // Retorna el tamaño de la lista
    override fun getItemCount(): Int {
        return characterList.size
    }
}










